export module MessageAlert {

  export class MessageAlert {

    text: string;
    type: AlertType;
    createdAt: Date;

    isSuccess: boolean;
    isInfo: boolean;
    isWarning: boolean;
    isDanger: boolean;

    constructor(text: string, type: AlertType = AlertType.Info) {
      this.text = text;
      this.setAlertType(type);
      this.createdAt = new Date();
    }

    private setAlertType(type: AlertType) {
      this.isSuccess = (type === AlertType.Success);
      this.isInfo = (type === AlertType.Info);
      this.isWarning = (type === AlertType.Warning);
      this.isDanger = (type === AlertType.Danger);
      this.type = type;
    }
  }

  export enum AlertType {
    Success,
    Info,
    Warning,
    Danger
  }
}
