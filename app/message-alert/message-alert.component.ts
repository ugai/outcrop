import {Component, Input, DoCheck, IterableDiffers} from 'angular2/core';
import {MessageAlert} from '../message-alert/message-alert';

@Component({
  selector: 'my-message-alert',
  template: `
    <div class="message-list">
      <div *ngFor="#message of messages">
        <div class="alert alert-custom" role="alert"
          [class.alert-success]="message.isSuccess"
          [class.alert-info]="message.isInfo"
          [class.alert-warning]="message.isWarning"
          [class.alert-danger]="message.isDanger">
          {{ message.text }}
          ({{ message.createdAt.toTimeString().split(' ')[0] }})
        </div>
      </div>
    </div>
  `,
  styles: [`
    .alert-custom {
      margin-top: .5em;
      padding-top: .25em;
      padding-bottom: .25em;
    }
  `]
})
export class MessageAlertComponent implements DoCheck {

  @Input() maxCount: number;
  @Input() messages: MessageAlert.MessageAlert[];
  public differ: any;

  constructor(differs: IterableDiffers) {
    this.maxCount = 3;
    this.differ = differs.find([]).create(null);
  }

  ngDoCheck(): void {

    let changes = this.differ.diff(this.messages);

    if (changes) {
      while (this.messages.length > this.maxCount) {
        this.messages.shift();
      }
    }
  }
}
