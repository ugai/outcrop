import {Component} from 'angular2/core';
import {UserAdminComponent} from './user-admin/user-admin.component';

@Component({
  selector: 'my-test-font-awesome',
  template: `
  <h2>UIフォント</h2>

  <h3>公式サンプル</h3>
  <p>
    <a href="https://fortawesome.github.io/Font-Awesome/examples/">https://fortawesome.github.io/Font-Awesome/examples/</a>
  </p>

  <h4>重ね</h4>
  <p>
    <span class="fa-stack fa-lg">
      <i class="fa fa-square fa-stack-2x"></i>
      <i class="fa fa-terminal fa-stack-1x fa-inverse"></i>
    </span>
    fa-terminal on fa-square<br>
  </p>

  <h4>回転</h4>
  <p>
    <i class="fa fa-spinner fa-spin"></i>
    <i class="fa fa-circle-o-notch fa-spin"></i>
    <i class="fa fa-refresh fa-spin"></i>
    <i class="fa fa-cog fa-spin"></i>
    <i class="fa fa-spinner fa-pulse"></i>
  </p>

  <h3>使えそうな文字</h3>
  <p>
    <a href="https://fortawesome.github.io/Font-Awesome/icons/#web-application">https://fortawesome.github.io/Font-Awesome/icons/#web-application</a>
  </p>
  <p>
    <i class="fa fa-arrow-left"></i>
    <i class="fa fa-arrow-right"></i>
    <br>
    <i class="fa fa-ban"></i>
    <i class="fa fa-bars"></i>
    <i class="fa fa-calendar"></i>
    <br>
    <i class="fa fa-check"></i>
    <i class="fa fa-check-circle"></i>
    <i class="fa fa-check-circle-o"></i>
    <br>
    <i class="fa fa-circle"></i>
    <i class="fa fa-circle-o"></i>
    <br>
    <i class="fa fa-clock-o"></i>
    <i class="fa fa-cogs"></i>
    <i class="fa fa-comments"></i>
    <i class="fa fa-download"></i>
    <i class="fa fa-envelope"></i>
    <i class="fa fa-eraser"></i>
    <i class="fa fa-floppy-o"></i>
    <i class="fa fa-undo"></i>
    <br>
    <i class="fa fa-exclamation"></i>
    <i class="fa fa-exclamation-circle"></i>
    <i class="fa fa-exclamation-triangle"></i>
    <br>
    <i class="fa fa-info"></i>
    <i class="fa fa-info-circle"></i>
    <br>
    <i class="fa fa-plus"></i>
    <i class="fa fa-minus"></i>
    <br>
    <i class="fa fa-pencil"></i>
    <i class="fa fa-pencil-square-o"></i>
    <br>
    <i class="fa fa-print"></i>
    <br>
    <i class="fa fa-question"></i>
    <i class="fa fa-question-circle"></i>
    <br>
    <i class="fa fa-refresh"></i>
    <i class="fa fa-search"></i>
    <i class="fa fa-send"></i>
    <br>
    <i class="fa fa-sign-in"></i>
    <i class="fa fa-sign-out"></i>
    <br>
    <i class="fa fa-sliders"></i>
    <i class="fa fa-sort-alpha-asc"></i>
    <br>
    <i class="fa fa-star"></i>
    <i class="fa fa-star-half-o"></i>
    <i class="fa fa-star-o"></i>
    <br>
    <i class="fa fa-thumb-tack"></i>
    <i class="fa fa-times"></i>
    <i class="fa fa-trash"></i>
    <i class="fa fa-user"></i>
    <i class="fa fa-users"></i>
    <br>
    <i class="fa fa-jpy"></i>
  </p>
  `
})
export class TestFontAwesomeComponent {}

@Component({
  selector: 'my-test-color',
  template: `
  <h2>配色</h2>
  <div class="row">
    <div class="col-sm-1"></div>
    <div class="col-sm-2 my-blue-bg">blue</div>
    <div class="col-sm-2 my-green-bg">green</div>
    <div class="col-sm-2 my-yellow-bg">yellow</div>
    <div class="col-sm-2 my-red-bg">red</div>
    <div class="col-sm-2 my-purple-bg">purple</div>
    <div class="col-sm-1"></div>
  </div>
  <div class="row my-blue-bg">blue</div>
  <div class="row my-green-bg">green</div>
  <div class="row my-yellow-bg">yellow</div>
  <div class="row my-red-bg">red</div>
  <div class="row my-purple-bg">purple</div>
  `
})
export class TestColorComponent {}

@Component({
  selector: 'my-test',
  template: `
  <div class="container">

    <h1>{{ title }}</h1>

    <my-test-color></my-test-color>
    <hr>
    <my-test-font-awesome></my-test-font-awesome>
    <hr>
    <my-user-admin></my-user-admin>

  </div>
  `,
  directives: [
    TestColorComponent,
    TestFontAwesomeComponent,
    UserAdminComponent
  ]
})
export class TestComponent {
  public title: string = "テスト";
}
