import {Component} from 'angular2/core';

@Component({
  selector: 'my-welcome',
  template: `
  <div class="container">

    <div class="row">
      <h1>Outcrop</h1>
      <p class="lead">
        Angular2の私的サンプル
      </p>
    </div>

    <hr>

    <div class="row">
      <div class="col-xl-6">

        <div class="row">
          <div class="col-md-6">
            <h2>使ってるフレームワーク</h2>
            <ul class="list-style">
              <li><a href="https://angular.io/">Angular2</a></li>
              <li><a href="http://v4-alpha.getbootstrap.com/">Bootstrap4</a></li>
            </ul>
          </div>
          <div class="col-md-6">
            <h2>借りてるサーバ</h2>
            <ul class="list-style">
              <li><a href="https://www.openshift.com/">OpenShift Online</a></li>
              <li><a href="http://wercker.com/">Wercker CI</a></li>
              <li><a href="https://bitbucket.org/">Bitbucket (Git)</a></li>
            </ul>
          </div>
        </div>

        <hr>

        <h2>動作環境</h2>

        <p class="lead">
          <span class="label label-success">
            対応&nbsp;
            <i class="fa fa-chrome"></i>
          </span>
          <span class="label label-warning">
            あやしい…&nbsp;
            <i class="fa fa-edge"></i>
            <i class="fa fa-firefox"></i>
          </span>
          <span class="label label-danger">
            R.I.P.&nbsp;
            <i class="fa fa-internet-explorer"></i>
          </span>
        </p>
        <p>最新のChromeのみで動作を確認。現在はIE11、Edge、Firefoxには非対応。</p>

        <hr>

        <h2>開発環境</h2>

        <p>Windows 10にNode.jsを入れてAtomでコーディング。</p>
        <p>変更をBitbucketへPushすると勝手にOpenShiftへデプロイするように組んでいます。
        <code>ローカルGit → Bitbucket → Wercker → OpenShift</code>です。
        全部タダで借りられるのでおすすめ。</p>
      </div>
      <div class="col-xl-6 text-xs-center">
        <figure>
          <img width="533px" class="img-rounded" alt="San Carlos Water, Falkland Islands"
            src="/images/San_Carlos_Water,_Falkland_Islands.jpg">
          <figcaption>
            By Chris Pearson from Leuchars, Scotland (View over San Carlos Sound)
            [<a href="http://creativecommons.org/licenses/by/2.0">CC BY 2.0</a>],
            <a href="https://commons.wikimedia.org/wiki/File%3ASan_Carlos_Water%2C_Falkland_Islands.jpg">via Wikimedia Commons</a>
          </figcaption>
        </figure>
      </div>
    </div>
  </div>
  `
})
export class WelcomeComponent {
}
