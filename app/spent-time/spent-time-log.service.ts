import {Injectable} from 'angular2/core';

import {SpentTimeLog} from './spent-time-log';
import {SPENT_TIME_LOGS} from './mock-spent-time-logs';
import {DAY_AS_TIME} from '../common/consts';

@Injectable()
export class SpentTimeLogService {

  getSpentTimeLogs(userId: string, startDate: Date, endDate: Date): Promise<SpentTimeLog[]> {

    // drop object references
    let startDateClone: Date = new Date(startDate.getTime());
    let endDateClone: Date = new Date(endDate.getTime());

    // truncate time fields
    startDateClone.setHours(0,0,0,0);
    endDateClone.setHours(0,0,0,0);

    return Promise.resolve(this.generateMock(startDateClone, endDateClone));
  }

  private generateMock(startDate: Date, endDate: Date): SpentTimeLog[] {

    let spentTimeLogs: SpentTimeLog[] = SPENT_TIME_LOGS;

    for (let i=0; i<spentTimeLogs.length; i++) {
      let spentTimes = spentTimeLogs[i].spentTimes;
      let j;

      for (j=0; j<spentTimes.length; j++) {
        spentTimes[j].date = new Date(startDate.getTime() + DAY_AS_TIME * j);
        if (spentTimes[j].date.getTime() > endDate.getTime()) {
          break;
        }
      }

      if (j < spentTimes.length) {
        // drop unnecessary date records
        spentTimes.splice(j);
      }
    }

    return spentTimeLogs;
  }
}
