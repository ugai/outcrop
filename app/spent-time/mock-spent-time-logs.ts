import {SpentTimeLog} from './spent-time-log';

export let SPENT_TIME_LOGS: SpentTimeLog[] = [
  {
    projectId: 1, taskId: 1,
    spentTimes: [
      {date: null, spentTime: 1},
      {date: null, spentTime: 2},
      {date: null, spentTime: 3},
      {date: null, spentTime: 4},
      {date: null, spentTime: 5},
      {date: null, spentTime: 6},
      {date: null, spentTime: 7}
    ]
  },
  {
    projectId: 2, taskId: 11,
    spentTimes: [
      {date: null, spentTime: 2},
      {date: null, spentTime: 3},
      {date: null, spentTime: 4},
      {date: null, spentTime: 5},
      {date: null, spentTime: 6},
      {date: null, spentTime: 7},
      {date: null, spentTime: 8}
    ]
  }
];
