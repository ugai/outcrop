import {Component, Input, Output, EventEmitter} from 'angular2/core';
import {NgForm} from 'angular2/common';

import {SpentTimeLog, SpentTime} from './spent-time-log';
import {Project} from './project';
import {Task} from './task';
import {CalendarDate} from '../calendar-date/calendar-date';

@Component({
  selector: 'my-spent-time-log',
  template: `
  <div class="row">
    <div class="col-sm-5">
      <div class="row">
        <div class="col-sm-1">
          <button type="button" class="close" aria-label="Remove"
            (click)="onClickRemove()">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="col-sm-6">
          <select class="form-control form-control-sm" required
            (change)="onChangeProject()"
            [(ngModel)]="log.projectId">
            <option *ngFor="#project of projects" [value]="project.id">
              {{ project.name }}
            </option>
          </select>
        </div>
        <div class="col-sm-5">
          <select class="form-control form-control-sm" required
            [disabled]="!log.projectId"
            (change)="onChangeTask()"
            [(ngModel)]="log.taskId">
            <option></option>
            <option *ngFor="#task of taskByProjectId[log.projectId]" [value]="task.id"
              [disabled]="task.isSelected">
              {{ task.name }}
            </option>
          </select>
        </div>
      </div>
    </div>
    <div class="col-sm-1" *ngFor="#spentTime of log.spentTimes">
        <input type="number" class="form-control form-control-sm"
          min="0" max="24" step="0.25"
          [(ngModel)]="spentTime.spentTime">
    </div>
  </div>

  `
})
export class SpentTimeLogComponent {

  @Input() log: SpentTimeLog;
  @Input() projects: Project[];
  @Input() taskByProjectId: { [key:number]: Task[] };
  @Input() calendar: CalendarDate[];

  @Output() removeRequest: EventEmitter<SpentTimeLog> = new EventEmitter();
  @Output() updateTaskRequest: EventEmitter<any> = new EventEmitter();

  constructor() {}

  onChangeProject(): void {
    this.log.taskId = null;
    this.updateTaskRequest.emit(null);
  }

  onChangeTask(): void {
    this.updateTaskRequest.emit(null);
  }

  onClickRemove(): void {
    this.removeRequest.emit(this.log);
  }
}
