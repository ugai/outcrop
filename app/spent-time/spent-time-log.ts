export interface SpentTimeLog {
  projectId: number;
  taskId: number;
  spentTimes: SpentTime[];
}

export interface SpentTime {
  date?: Date;
  spentTime: number;
}
