import {Injectable} from 'angular2/core';

import {Task} from './Task';
import {TASKS} from './mock-tasks';

@Injectable()
export class TaskService {
  getTasks(): Promise<Task[]> {
    return Promise.resolve(TASKS);
  }
}
