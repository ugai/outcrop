export interface Task {
  id: number;
  projectId: number;
  code: string;
  name: string;
  isSelected?: boolean;
}
