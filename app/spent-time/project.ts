export interface Project {
  id: number;
  code: string;
  name: string;
}
