import {Component, OnInit} from 'angular2/core';
import {NgForm} from 'angular2/common';
import {Observable} from 'rxjs/Observable';
import 'rxjs/Rx';

import {DateBigEndianPipe} from '../common/date-big-endian.pipe';
import {SpentTimeLogComponent} from './spent-time-log.component';

import {SpentTimeLog, SpentTime} from './spent-time-log';
import {Project} from './project';
import {Task} from './task';
import {CalendarDate} from '../calendar-date/calendar-date';

import {ProjectService} from './project.service';
import {TaskService} from './task.service';
import {SpentTimeLogService} from './spent-time-log.service';
import {CalendarDateService} from '../calendar-date/calendar-date.service';

import {DAY_AS_TIME} from '../common/consts';

@Component({
  selector: 'my-spent-time-main',
  template: `
  <div class="container" id="spent-time">
    <h1>{{ title }}</h1>
    <form (ngSubmit)="onSubmit()" #logsForm="ngForm">

      <nav>
        <ul class="pager">
          <li class="pager-prev"><a href="#">前の週</a></li>
          <li class="pager-next"><a href="#">次の週</a></li>
        </ul>
      </nav>

      <div class="row">
        <div class="pull-sm-right">
          <button class="btn btn-secondary" type="button"
            (click)="addLog()">行追加</button>
        </div>
      </div>

      <div class="log-list">
        <div class="row log-list-header text-xs-center">
          <div class="col-sm-5">
            <div class="row">
              <div class="col-sm-7">プロジェクト</div>
              <div class="col-sm-5">タスク</div>
            </div>
          </div>
          <div class="col-sm-1" *ngFor="#d of calendar">
            <span [class.holiday]="d.isHoliday">{{ d.date | dateBigEndian:'month':'day':'daysOfWeek' }}</span>
          </div>
        </div>
        <my-spent-time-log *ngFor="#log of logs"
          (removeRequest)="removeLog($event)"
          (updateTaskRequest)="updateTaskSelectionStatus($event)"
          [log]="log" [projects]="projects" [taskByProjectId]="taskByProjectId" [calendar]="calendar">
        </my-spent-time-log>
        <hr>
        <div class="row text-xs-right">
          <div class="col-sm-5">間接</div>
          <div class="col-sm-1">1</div>
          <div class="col-sm-1">2</div>
          <div class="col-sm-1">3</div>
          <div class="col-sm-1">4</div>
          <div class="col-sm-1">5</div>
          <div class="col-sm-1">6</div>
          <div class="col-sm-1">7</div>
        </div>
        <div class="row text-xs-right">
          <div class="col-sm-5">合計</div>
          <div class="col-sm-1">1</div>
          <div class="col-sm-1">2</div>
          <div class="col-sm-1">3</div>
          <div class="col-sm-1">4</div>
          <div class="col-sm-1">5</div>
          <div class="col-sm-1">6</div>
          <div class="col-sm-1">7</div>
        </div>
      </div>

      <div class="row">
        <div class="pull-sm-right">
          <button class="btn btn-secondary" type="button" (click)="onClickReload()">再読み込み</button>
          <button class="btn btn-primary" type="submit">保存</button>
        </div>
      </div>

      <!-- TODO: remove -->
      <div class="row" *ngIf="reloadedTime">
        <p>Reloaded at {{ reloadedTime }} !</p>
      </div>
      <div class="row" *ngIf="submittedTime">
        <p>Submitted at {{ submittedTime }} !</p>
      </div>

    </form>
  </div>
  `,
  directives: [SpentTimeLogComponent],
  providers: [
    ProjectService,
    TaskService,
    SpentTimeLogService,
    CalendarDateService
  ],
  pipes: [DateBigEndianPipe]
})
export class SpentTimeMainComponent implements OnInit {

  public title: string = "工数";

  private reloadedTime: Date; // TODO: remove
  private submittedTime: Date; // TODO: remove

  private _days: number = 7;
  private _startDate: Date;
  private _endDate: Date;

  public projects: Project[];
  public tasks: Task[];
  public logs: SpentTimeLog[];
  public calendar: CalendarDate[];

  public taskByProjectId: any = {};

  constructor(
    private _projectService: ProjectService,
    private _taskService: TaskService,
    private _spentTimeLogService: SpentTimeLogService,
    private _calendarDateService: CalendarDateService
  ) {
    this._startDate = new Date(new Date().getTime() - DAY_AS_TIME * new Date().getDay());
    this._startDate.setHours(0,0,0,0);
    this._endDate = new Date(this._startDate.getTime() + DAY_AS_TIME * (this._days - 1));
  }

  ngOnInit(): void {
    this.getInitialData();
  }

  onClickReload(): void {
    this.reloadedTime = new Date(); // TODO: remove
  }

  onSubmit(): void {
    this.submittedTime = new Date(); // TODO: remove
  }

  getInitialData(): void {

    let source: any = Observable.forkJoin(
      this._calendarDateService.getCalendarDates(this._startDate, this._endDate),
      this._spentTimeLogService.getSpentTimeLogs(null, this._startDate, this._endDate),
      this._projectService.getProjects(),
      this._taskService.getTasks()
    );

    source.subscribe(data => {
      this.calendar = <CalendarDate[]>data[0];
      this.logs = <SpentTimeLog[]>data[1];
      this.projects = <Project[]>data[2];
      this.tasks = <Task[]>data[3];
      this.createTaskByProjectMap();
    });
  }

  addLog(): void {

    let log: SpentTimeLog = { projectId: null, taskId: null, spentTimes: [] };

    // fill spentTimes
    for (let i=0; i<this._days; i++) {
      log.spentTimes.push(
        {date: new Date(this._startDate.getTime() + DAY_AS_TIME * i), spentTime: null}
      )
    }

    this.logs.push(log);
  }

  removeLog(log: SpentTimeLog): void {
    let i: number = this.logs.indexOf(log);

    if (i > -1) {
      this.logs.splice(i, 1);
    }
    this.updateTaskSelectionStatus();
  }

  createTaskByProjectMap(): void {
    this.tasks.forEach((task:Task): void => {
      let id = task.projectId;
      if (!Array.isArray(this.taskByProjectId[id])) {
        this.taskByProjectId[id] = [];
      }
      this.taskByProjectId[id].push(task);
    });
    this.updateTaskSelectionStatus();
  }

  updateTaskSelectionStatus(): void {
    let keys = Object.keys(this.taskByProjectId);

    // clear all status
    for (let i=0; i<keys.length; i++) {
      for (let j=0; j<this.taskByProjectId[keys[i]].length; j++) {
        this.taskByProjectId[keys[i]][j].isSelected = false;
      }
    }

    // update status
    this.logs.forEach((log:SpentTimeLog): void => {

      if (log.taskId == null) { return; }

      for (let i=0; i<keys.length; i++) {
        let projectId = keys[i];

        for (let j=0; j<this.taskByProjectId[projectId].length; j++) {
          let taskId = this.taskByProjectId[projectId][j].id;
          if (taskId == log.taskId) {
            this.taskByProjectId[projectId][j].isSelected = true;
            break;
          }
        }
      }
    }); // forEach

  }
}
