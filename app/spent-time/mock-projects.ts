import {Project} from './project';

export let PROJECTS: Project[] = [
  {id: 1, code: "CURR01", name: "カレー"},
  {id: 2, code: "SPAG01", name: "スパゲッティ"}
];
