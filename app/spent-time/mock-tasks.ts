import {Task} from './task';

export let TASKS: Task[] = [
  {id: 1, projectId: 1, code: "A001", name: "下ごしらえ"},
  {id: 2, projectId: 1, code: "B001", name: "煮込む"},
  {id: 3, projectId: 1, code: "C001", name: "盛り付け"},
  {id: 4, projectId: 1, code: "Z001", name: "隠し味"},
  {id: 11, projectId: 2, code: "A001", name: "茹で"},
  {id: 12, projectId: 2, code: "B001", name: "盛り付け"}
];
