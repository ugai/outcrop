export interface CalendarDate {
  date: Date;
  isHoliday: boolean;
  isPast: boolean;
  isToday: boolean;
  isFuture: boolean;
}
