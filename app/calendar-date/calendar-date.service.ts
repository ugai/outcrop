import {Injectable} from 'angular2/core';
import {Http, Response, URLSearchParams} from 'angular2/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/Rx';

import {CalendarDate} from './calendar-date';
import {Utils} from '../common/utils';

import {CLAST_BASE_URL} from '../common/consts';

@Injectable()
export class CalendarDateService {

  private _url = CLAST_BASE_URL + '/calendar_dates';

  constructor(private http: Http) {}

  getCalendarDates(startDate: Date, endDate: Date): Observable<any> {

    // drop object references
    let startDateClone: Date = new Date(startDate.getTime());
    let endDateClone: Date = new Date(endDate.getTime());

    // truncate time fields
    startDateClone.setHours(0,0,0,0);
    endDateClone.setHours(0,0,0,0);

    let params = new URLSearchParams();
    params.set('start', Utils.dateToStringYYYYMMDD(startDateClone));
    params.set('end', Utils.dateToStringYYYYMMDD(endDateClone));

    return this.http.get(this._url, { search: params })
      .map(res => <CalendarDate[]> JSON.parse(res.text(), Utils.dateTimeReviver))
      //.do(data => console.log(data))
      .catch(this.handleError)
    ;
  }

  private handleError(error: any): Observable<any> {
    console.error(error);
    return Observable.throw(error.json().error || 'Server error');
  }

  private generateMock(startDate: Date, endDate: Date): CalendarDate[] {

    let calendar = [];
    let nowDate = new Date(Date.now());

    // truncate time fields
    startDate.setHours(0,0,0,0);
    endDate.setHours(0,0,0,0);
    nowDate.setHours(0,0,0,0);

    for (let d=startDate; d<=endDate; d.setDate(d.getDate() + 1)) {

      calendar.push({
        date: new Date(d.getTime()),
        isHoliday: (d.getDay() === 0 || d.getDay() === 6),
        isPast: (d.getTime() < nowDate.getTime()),
        isToday: (d.getTime() === nowDate.getTime()),
        isFuture: (d.getTime() > nowDate.getTime())
      });
    }

    return calendar;
  }
}
