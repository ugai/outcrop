export interface AttendanceType {
  id: number;
  name: string;
  forHoliday: boolean;
  isTimeRequired?: boolean;
}
