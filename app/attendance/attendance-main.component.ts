import {Component, OnInit} from 'angular2/core';
import {NgForm} from 'angular2/common';
import {Observable} from 'rxjs/Observable';
import 'rxjs/Rx';

import {AttendanceLogComponent} from './attendance-log.component';
import {AttendanceToolbarComponent} from './attendance-toolbar.component';
import {MessageAlertComponent} from '../message-alert/message-alert.component';

import {AttendanceType} from './attendance-type';
import {AttendanceLog} from './attendance-log';
import {CalendarDate} from '../calendar-date/calendar-date';
import {MessageAlert} from '../message-alert/message-alert';

import {AttendanceTypeService} from './attendance-type.service';
import {AttendanceLogService} from './attendance-log.service';
import {CalendarDateService} from '../calendar-date/calendar-date.service';

import {DateBigEndianPipe} from '../common/date-big-endian.pipe';
import {DAY_AS_TIME} from '../common/consts';

@Component({
  selector: 'my-attendance-main',
  template: `
  <div class="container" id="attendance">
    <h1>{{ title }}</h1>
    <form (ngSubmit)="onSubmit()">

      <my-attendance-toolbar
        (reloadRequest)="reload()"
        (changeMonthAbsoluteRequest)="changeMonthAbsolute($event)"
        (changeMonthRelativeRequest)="changeMonthRelative($event)"
        (fillAllRequest)="fillAll()"
        (clearAllRequest)="clearAll()"
        [today]="_today"
        [startDate]="_startDate" [endDate]="_endDate"
        [year]="year" [month]="month" [logs]="logs">
      </my-attendance-toolbar>

      <my-message-alert *ngIf="messages" [messages]="messages" [maxCount]="1">
      </my-message-alert>

      <div class="log-list">
        <div class="row log-list-header text-xs-center">
          <div class="col-sm-3">日付</div>
          <div class="col-sm-3">勤務区分</div>
          <div class="col-sm-3">出勤時刻</div>
          <div class="col-sm-3">退勤時刻</div>
        </div>

        <my-attendance-log *ngIf="logs" *ngFor="#log of logs" [log]="log" [attendanceTypes]="attendanceTypes">
        </my-attendance-log>

        <div class="text-xs-center" *ngIf="!logs">
          <i class="fa fa-circle-o-notch fa-spin"></i> Loading...
        </div>
      </div>

      <!-- 時刻の入力補助 -->
      <datalist id="typical-time-values">
        <option value="08:30">
        <option value="17:30">
      </datalist>
    </form>
  </div>
  `,
  styles: [`
    .toolbar-addon {
      padding-left:  .25em;
      padding-right: .5em;
    }
  `],
  directives: [
    AttendanceLogComponent,
    AttendanceToolbarComponent,
    MessageAlertComponent
  ],
  providers: [
    AttendanceTypeService,
    AttendanceLogService,
    CalendarDateService
  ],
  pipes: [DateBigEndianPipe]
})
export class AttendanceMainComponent implements OnInit {

  public title: string = "勤怠";

  private _today: Date;
  private _startDate: Date;
  private _endDate: Date;

  public year: number;
  public month: number;

  public attendanceTypes: AttendanceType[];
  public logs: AttendanceLog[];
  public calendar: CalendarDate[];

  public messages: MessageAlert.MessageAlert[];

  constructor(
    private _attendanceTypeService: AttendanceTypeService,
    private _attendanceLogService: AttendanceLogService,
    private _calendarDateService: CalendarDateService
  ) {
    this._today = new Date();
    this._today.setHours(0,0,0,0);
    this.year = this._today.getFullYear();
    this.month = this._today.getMonth() + 1;
    this._startDate = new Date(this.year, this.month - 1, 1);
    this._endDate = new Date(this.year, (this.month - 1) + 1, 0);
    this.messages = [];
  }

  ngOnInit(): void {
    this.getInitialData();
  }

  onSubmit(): void {
    this.setAttendanceLogs();
    this.messages.push(new MessageAlert.MessageAlert("保存しました。", MessageAlert.AlertType.Info));
  }

  getInitialData(): void {

    let source: any = Observable.forkJoin(
      this._calendarDateService.getCalendarDates(this._startDate, this._endDate),
      this._attendanceLogService.getAttendanceLogs(null, this._startDate, this._endDate),
      this._attendanceTypeService.getAttendanceTypes()
    );

    source.subscribe(data => {
      this.calendar = <CalendarDate[]>data[0];
      this.logs = <AttendanceLog[]>data[1];
      this.attendanceTypes = <AttendanceType[]>data[2];
      this.fillSparseLogDates();
      this.messages.push(new MessageAlert.MessageAlert("取得しました。", MessageAlert.AlertType.Info));
    });
  }

  setAttendanceLogs(): void {
    if (!this.logs) { return; }
    this._attendanceLogService.setAttendanceLogs(
        this.logs.filter((log) => !log.isFuture)
      )
      .subscribe(
        data => console.log(data),
        error => console.error(error)
      );
  }

  reload(): void {

    this.logs = null;
    this._startDate = new Date(this.year, this.month - 1, 1);
    this._endDate = new Date(this.year, (this.month - 1) + 1, 0);

    let source: any = Observable.forkJoin(
      this._calendarDateService.getCalendarDates(this._startDate, this._endDate),
      this._attendanceLogService.getAttendanceLogs(null, this._startDate, this._endDate)
    );

    source.subscribe(data => {
      this.calendar = <CalendarDate[]>data[0];
      this.logs = <AttendanceLog[]>data[1];
      this.fillSparseLogDates();
      this.messages.push(new MessageAlert.MessageAlert("再取得しました。", MessageAlert.AlertType.Info));
    });
  }

  changeMonthAbsolute(newDate: Date): void {
    if (newDate == null) { return; }
    this.year = newDate.getFullYear();
    this.month = newDate.getMonth() + 1;
    this.reload();
  }

  changeMonthRelative(amount: number): void {
    if (amount == null) {
      // change to current date
      this._today = new Date();
      this._today.setHours(0,0,0,0);
      this.year = this._today.getFullYear();
      this.month = this._today.getMonth() + 1;
    }

    let d = new Date(this.year, (this.month - 1) + amount, 1);
    this.year = d.getFullYear();
    this.month = d.getMonth() + 1;
    this.reload();
  }

  fillAll() {
    for (let i=0; i<this.logs.length; i++) {
      let log = this.logs[i];

      if (log.isFuture) { break; }
      if (log.attendanceTypeId || log.startTime || log.endTime) { continue; }

      let typeId: number = null;
      let startTime: string = null;
      let endTime: string = null;

      // TODO: use Array.prototype.find()
      let type: AttendanceType = this.attendanceTypes.filter(
        (x: AttendanceType) => log.isHoliday === x.forHoliday).shift();

      if (type && type.id) {
        typeId = type.id;
      }

      if (!log.isHoliday) {
        startTime = "08:30"
        endTime = "17:30"
      }

      log.attendanceTypeId = typeId;
      log.startTime = startTime;
      log.endTime = endTime;
    }
    this.messages.push(new MessageAlert.MessageAlert("自動で入力しました。", MessageAlert.AlertType.Success));
  }

  clearAll() {
    for (let i=0; i<this.logs.length; i++) {
      let log = this.logs[i];

      if (log.isFuture) { break; }

      log.attendanceTypeId = null;
      log.startTime = null;
      log.endTime = null;
    }
    this.messages.push(new MessageAlert.MessageAlert("クリアしました。", MessageAlert.AlertType.Success));
  }

  /**
   * fill sparse date log recrods by calendar dates
   */
  fillSparseLogDates(): void {

    for (let i=0; i<this.calendar.length; i++) {
      let c: CalendarDate = this.calendar[i];

      let closestLogIndex: number = -1;
      let hasLog: boolean = false;

      for (let j=0; j<this.logs.length; j++) {
        let l: AttendanceLog = this.logs[j];

        if (l.date.getTime() === c.date.getTime()) {
          l.isHoliday = c.isHoliday;
          l.isPast = c.isPast;
          l.isToday = c.isToday;
          l.isFuture = c.isFuture;
          closestLogIndex = j;
          hasLog = true;
          break;
        } else if (l.date.getTime() < c.date.getTime()) {
          closestLogIndex = j;
        }
      }

      if (!hasLog) {
        let newLog: AttendanceLog = {
          date: c.date,
          attendanceTypeId: null,
          isHoliday: c.isHoliday,
          isPast: c.isPast,
          isToday: c.isToday,
          isFuture: c.isFuture
        };
        this.logs.splice(closestLogIndex+1, 0, newLog);
      }
    }
  }

}
