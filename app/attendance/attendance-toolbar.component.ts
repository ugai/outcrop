import {Component, Input, Output, EventEmitter, OnInit} from 'angular2/core';
import {AttendanceLog} from './attendance-log';

@Component({
  selector: 'my-attendance-toolbar',
  template: `
  <div class="btn-toolbar" role="toolbar" aria-label="ツールバー">
    <div class="btn-group" aria-label="年月移動">
      <button type="button" class="btn btn-secondary" (click)="changeMonthRelative($event, -1)">
        <i class="fa fa-arrow-left"></i>
      </button>
      <button type="button" class="btn btn-secondary"(click)="changeMonthRelative($event, null)"
        [class.active]="isCurrentMonth()">
        <i class="fa fa-circle-o"></i>
      </button>
      <button type="button" class="btn btn-secondary" (click)="changeMonthRelative($event, 1)">
        <i class="fa fa-arrow-right"></i>
      </button>
    </div>
    <div class="btn-group" aria-label="年月選択">
      <div class="form-inline">
        <select class="c-select form-control-sm" [(ngModel)]="year" (change)="changeMonthAbsolute($event.target.value, month)">
          <option *ngFor="#year of yearOptions" [value]="year">{{ year }}</option>
        </select>
        <span class="toolbar-addon">年</span>
        <select class="c-select form-control-sm" [(ngModel)]="month" (change)="changeMonthAbsolute(year, $event.target.value)">
          <option *ngFor="#month of monthOptions" [value]="month">{{ month }}</option>
        </select>
        <span class="toolbar-addon">月</span>
      </div>
    </div>
    <div class="btn-group" aria-label="その他">
      <div class="form-inline">
        <button type="button" class="btn btn-secondary" (click)="fillAll()" [disabled]="!logs">
          <i class="fa fa-rocket"></i>&nbsp;&nbsp;自動入力
        </button>
        <button type="button" class="btn btn-warning" (click)="clearAll()" [disabled]="!logs">
          <i class="fa fa-eraser"></i>&nbsp;&nbsp;消去
        </button>
      </div>
    </div>
    <div class="btn-group pull-sm-right" aria-label="保存">
      <button class="btn btn-primary" type="submit" [disabled]="!logs">
        <i class="fa fa-floppy-o"></i>&nbsp;&nbsp;保存
      </button>
    </div>
  </div>
  `,
  styles: [`
    .toolbar-addon {
      padding-left:  .25em;
      padding-right: .5em;
    }
  `]
})
export class AttendanceToolbarComponent implements OnInit {

  public yearOptions: number[] = [];
  public monthOptions: number[] = [1,2,3,4,5,6,7,8,9,10,11,12];

  @Input() today: Date;
  @Input() startDate: Date;
  @Input() endDate: Date;
  @Input() year: number;
  @Input() month: number;
  @Input() logs: AttendanceLog[];

  @Output() reloadRequest: EventEmitter<any> = new EventEmitter();
  @Output() changeMonthAbsoluteRequest: EventEmitter<Date> = new EventEmitter();
  @Output() changeMonthRelativeRequest: EventEmitter<number> = new EventEmitter();
  @Output() fillAllRequest: EventEmitter<any> = new EventEmitter();
  @Output() clearAllRequest: EventEmitter<any> = new EventEmitter();

  constructor() {}

  ngOnInit(): void {
    for (let i=-1; i<=1; i++) {
      this.yearOptions.push(this.today.getFullYear() + i);
    }
  }

  reload(): void {
    this.reloadRequest.emit(null);
  }

  changeMonthAbsolute(newYear: string, newMonth: string): void {
    let d = new Date(+newYear, +newMonth - 1);
    this.changeMonthAbsoluteRequest.emit(d);
  }

  changeMonthRelative(event: Event, amount: number): void {
    event.preventDefault(); // cancel event
    this.changeMonthRelativeRequest.emit(amount);
  }

  fillAll(): void {
    this.fillAllRequest.emit(null);
  }

  clearAll(): void {
    this.clearAllRequest.emit(null);
  }

  isCurrentMonth(): boolean {
    return (
      this.startDate.getFullYear() === this.today.getFullYear() &&
      this.startDate.getMonth() === this.today.getMonth()
    );
  }
}
