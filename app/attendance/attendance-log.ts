export interface AttendanceLog {
  date: Date;
  attendanceTypeId: number;
  isTimeRequired?: boolean;
  startTime?: string;
  endTime?: string;
  isHoliday?: boolean;
  isPast?: boolean;
  isToday?: boolean;
  isFuture?: boolean;
}
