import {Injectable} from 'angular2/core';
import {Http, Response, URLSearchParams, Headers, RequestOptions} from 'angular2/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/Rx';

import {AttendanceLog} from './attendance-log';
import {ATTENDANCE_LOGS} from './mock-attendance-logs';
import {DAY_AS_TIME, CLAST_BASE_URL} from '../common/consts';
import {Utils} from '../common/utils';


@Injectable()
export class AttendanceLogService {

  private _url = CLAST_BASE_URL + '/attendance_logs';

  constructor(private http: Http) {}

  getAttendanceLogs(userId: string, startDate: Date, endDate: Date): Observable<any> {

    // drop object references
    let startDateClone: Date = new Date(startDate.getTime());
    let endDateClone: Date = new Date(endDate.getTime());

    // truncate time fields
    startDateClone.setHours(0,0,0,0);
    endDateClone.setHours(0,0,0,0);

    let params = new URLSearchParams();
    params.set('start', Utils.dateToStringYYYYMMDD(startDateClone));
    params.set('end', Utils.dateToStringYYYYMMDD(endDateClone));

    return this.http.get(this._url, { search: params })
      .map(res => <AttendanceLog[]> JSON.parse(res.text(), Utils.dateTimeReviver))
      //.do(data => console.log(data))
      .catch(this.handleError)
    ;
  }

  setAttendanceLogs(logs: AttendanceLog[]): Observable<any> {
    let body = JSON.stringify(logs); // FIXME: "We may be able to skip the stringify step in the near future."
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });

    console.log(body)

    return this.http.put(this._url, body, options)
      .map(res => res.text()) // TODO: use JSON
      .catch(this.handleError);
  }

  private handleError(error: any): Observable<any> {
    console.error(error);
    return Observable.throw(error.json().error || 'Server error');
  }

  private generateMock(startDate: Date, endDate: Date): AttendanceLog[] {

    let attendanceLogs: AttendanceLog[] = ATTENDANCE_LOGS;
    let i;

    for (i=0; i<attendanceLogs.length; i++) {
      attendanceLogs[i].date = new Date(startDate.getTime() + DAY_AS_TIME * i);

      if (attendanceLogs[i].date.getTime() > endDate.getTime()) {
        break;
      }
    }

    if (i < attendanceLogs.length) {
      // drop unnecessary date records
      attendanceLogs.splice(i);
    }

    // drop some records for testing
    attendanceLogs.splice(5, 1);
    attendanceLogs.splice(10, 2);
    attendanceLogs.splice(15, 3);

    return attendanceLogs;
  }
}
