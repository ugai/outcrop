import {AttendanceType} from './attendance-type';

export let ATTENDANCE_TYPES: AttendanceType[] = [
  {id: 1, name: "出勤", forHoliday: false, isTimeRequired: true},
  {id: 2, name: "休日", forHoliday: true, isTimeRequired: false},
  {id: 3, name: "休出", forHoliday: true, isTimeRequired: true},
  {id: 4, name: "休暇", forHoliday: false, isTimeRequired: false}
];
