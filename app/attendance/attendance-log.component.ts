import {Component, Input, OnInit, DoCheck, KeyValueDiffers} from 'angular2/core';
import {NgForm} from 'angular2/common';

import {DateBigEndianPipe} from '../common/date-big-endian.pipe';
import {AttendanceType} from './attendance-type';
import {AttendanceLog} from './attendance-log';

import {Utils} from '../common/utils';

@Component({
  selector: 'my-attendance-log',
  template: `

    <div class="row" *ngIf="log">

      <hr class="hidden-sm-up">

      <div class="col-sm-3 text-xs-center">
        <span [class.holiday]="log.isHoliday">
          {{ log.date | dateBigEndian:'year':'month':'day':'daysOfWeek' }}
        </span>
      </div>

      <div class="col-sm-3">
        <select class="form-control form-control-sm"
          [disabled]="log.isFuture"
          [required]="log.isPast"
          [(ngModel)]="log.attendanceTypeId">
          <option></option>
          <option *ngFor="#attendanceType of attendanceTypes" [value]="attendanceType.id"
            [hidden]="log.isHoliday !== attendanceType.forHoliday">
            {{ attendanceType.name }}
          </option>
        </select>
      </div>

      <div class="col-sm-3">
        <input type="text" class="form-control form-control-sm"
          placeholder="--:--" list="typical-time-values"
          pattern="(0[6-9]|[12][0-9]):[0-5][0-9]"
          [disabled]="!log.isTimeRequired || log.isFuture"
          [required]="log.isTimeRequired && log.isPast"
          (blur)="onBlurTime($event, 'startTime')"
          [(ngModel)]="log.startTime">
      </div>

      <div class="col-sm-3">
        <input type="text" class="form-control form-control-sm"
          placeholder="--:--" list="typical-time-values"
          pattern="(0[6-9]|[12][0-9]):[0-5][0-9]"
          (blur)="onBlurTime($event, 'endTime')"
          [disabled]="!log.isTimeRequired || log.isFuture"
          [required]="log.isTimeRequired && log.isPast"
          [(ngModel)]="log.endTime">
      </div>
    </div>
  `,
  styles: [`
  `],
  pipes: [DateBigEndianPipe]
})
export class AttendanceLogComponent implements OnInit, DoCheck {

  @Input() log: AttendanceLog;
  @Input() attendanceTypes: AttendanceType[];
  public differ: any;

  constructor(differs: KeyValueDiffers) {
    this.differ = differs.find({}).create(null);
  }

  ngOnInit(): void {
    if (this.log) {
      this.updateAttendanceTypeInfo(this.log.attendanceTypeId);
    }
  }

  ngDoCheck(): void {

    let changes = this.differ.diff(this.log);

    if (changes) {
      //changes.forEachAddedItem(r => console.log('added: ' + r.item));
      //changes.forEachRemovedItem(r => console.log('removed: ' + r.item));
      changes.forEachChangedItem(r => {
        //console.log(`{'${r.key}': '${JSON.stringify(r.previousValue)}' to '${JSON.stringify(r.currentValue)}'}`);
        if (r.key === 'attendanceTypeId') {
          this.updateAttendanceTypeInfo(r.currentValue);
        }
      });
    }
  }

  onBlurTime(event: Event, modelName: string): void {

    let target = <HTMLInputElement> event.target;

    // fix format
    target.value = Utils.fixTimeStringFormat(target.value);
    this.log[modelName] = target.value; // update model

    // custom validation
    if (target.validity.patternMismatch) {
      target.setCustomValidity("入力可能な時刻は06:00～29:59です。");
    } else {
      target.setCustomValidity("");
    }
  }

  /**
   * update log status by attendance type
   * @param {number} id: attendance type id
   */
  updateAttendanceTypeInfo(id: number): void {

    let i;
    for (i=0; i<this.attendanceTypes.length; i++) {
      if (id == null || this.attendanceTypes[i].id == id) {
        break;
      }
    }

    // when unmatched
    if (i === this.attendanceTypes.length) { id = null };

    this.log.isTimeRequired = (
      id && this.attendanceTypes[i] && this.attendanceTypes[i].isTimeRequired
    );

    if (!this.log.isTimeRequired) {
      this.log.startTime = "";
      this.log.endTime = "";
    }
  }

}
