import {Injectable} from 'angular2/core';
import {Http, Response} from 'angular2/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/Rx';

import {AttendanceType} from './attendance-type';
import {ATTENDANCE_TYPES} from './mock-attendance-types';
import {Utils} from '../common/utils';

import {CLAST_BASE_URL} from '../common/consts';

@Injectable()
export class AttendanceTypeService {

  private _url = CLAST_BASE_URL + '/attendance_types';

  constructor(private http: Http) {}

  getAttendanceTypes(): Observable<any> {

    return this.http.get(this._url)
      .map(res => <AttendanceType[]> JSON.parse(res.text(), Utils.dateTimeReviver))
      //.do(data => console.log(data))
      .catch(this.handleError)
    ;
  }

  private handleError(error: any): Observable<any> {
    console.error(error);
    return Observable.throw(error.json().error || 'Server error');
  }
}
