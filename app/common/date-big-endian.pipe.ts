import {Pipe, PipeTransform} from 'angular2/core';

// ビッグエンディアンの日付書式パイプ
//   標準のDatePipeはバージョン2.0.0-beta.0（2016年1月）時点で
//   "en_US"ロケール固定なので、リトルエンディアンで表示される。
//   この問題を回避する。
@Pipe({
  name: 'dateBigEndian'
})
export class DateBigEndianPipe implements PipeTransform {

  private daysOfTheWeekNames: string[] = [
    "日","月","火","水","木","金","土"
  ];

  transform(value: Date, args: string[]) : string {

    if (value == null || args.length == 0) { return ""; }

    let yyyy: string = value.getFullYear()+"";
    let mm: string = ("0" + (value.getMonth() + 1)).slice(-2);
    let dd: string = ("0" + value.getDate()).slice(-2);
    let s: string = "";

    if (args.indexOf("year") >= 0) {
      s += ((s) ? '/' : '') + yyyy;
    }

    if (args.indexOf("month") >= 0) {
      s += ((s) ? '/' : '') + mm;
    }

    if (args.indexOf("day") >= 0) {
      s += ((s) ? '/' : '') + dd;
    }

    if (args.indexOf("daysOfWeek") >= 0) {
      s += ((s) ? ' ' : '') + "(" + this.daysOfTheWeekNames[value.getDay()] + ")"
    }

    return s
  }
}
