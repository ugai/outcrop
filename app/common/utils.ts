export module Utils {

  export interface Map<T> {
    [K: string]: T;
  }

  export function dateToStringYYYYMMDD(d: Date): string {

    const separator: string = '';
    let sb: any[] = [];

    sb.push(d.getFullYear());
    sb.push(('0' + (d.getMonth() + 1)).slice(-2));
    sb.push(('0' + d.getDate()).slice(-2));

    return sb.slice(0,3).join(separator);
  }

   /**
    * revive stringified Date for JSON.parse()
    * @param  {string} key   object key
    * @param  {any}    value object value
    * @return {any}          revived value
    */
  export function dateTimeReviver(key: string, value: any): any {

    if (typeof value === 'string') {
      let s: any = /^(\d{4})-(\d{2})-(\d{2})$/.exec(value);
      if (s) {
        return new Date(+s[1], s[2]-1, +s[3]);
      }
    }
    return value;
  }

  export function fixTimeStringFormat(s: string): string {

    let re: RegExp;

    // hours and minutes
    re = /^([0-1]?[0-9]|[2][0-3])([0-5][0-9])$/g;
    if (re.test(s)) {
      return ("0" + s.replace(re, "$1")).slice(-2) + ":" +
             ("0" + s.replace(re, "$2")).slice(-2);
    }

    // hours only
    re = /^([0-1]?[0-9]|[2][0-3])$/g;
    if (re.test(s)) {
      return ("0" + s.replace(re, "$1")).slice(-2) + ":00";
    }

    // minutes only
    re = /^([0-5]?[0-9])$/g;
    if (re.test(s)) {
      return "00:" + ("0" + s.replace(re, "$1")).slice(-2);
    }

    // hours and minutes
    re = /^([0-9])([0-9])$/g;
    if (re.test(s)) {
      return ("0" + s.replace(re, "$1")).slice(-2) + ":" +
             ("0" + s.replace(re, "$2")).slice(-2);
    }

    // zero-fill
    re = /^([0-9]{1,2}):([0-9]{1,2})$/g;
    if (re.test(s)) {
      return ("0" + s.replace(re, "$1")).slice(-2) + ":" +
             ("0" + s.replace(re, "$2")).slice(-2);
    }

    return s;
  }
}
