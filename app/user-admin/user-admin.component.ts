import {Component, OnInit, Input} from 'angular2/core';
import {NgForm} from 'angular2/common';

import {UserService} from './user.service';
import {User} from './user';

@Component({
  selector: 'my-user',
  template: `
    <div class="row">
      <div class="col-xs-2">{{ user.id }}</div>
      <div class="col-xs-2"><input type="text" class="form-control-sm" [(ngModel)]="user.name"></div>
      <div class="col-xs-3"><input type="email" class="form-control-sm" [(ngModel)]="user.email"></div>
    </div>
  `
})
export class UserComponent {
  @Input() user: User;
  constructor() {}
}

@Component({
  selector: 'my-user-admin',
  template: `
  <div class="container" id="attendance">
    <h1>{{ title }}</h1>

    <p>バックエンドサーバとの連携テスト</p>

    <form (ngSubmit)="onSubmit()">

      <div class="btn-toolbar" role="toolbar" aria-label="ツールバー">
        <div class="btn-group pull-sm-right" aria-label="入出力">
          <button class="btn btn-secondary" type="button" (click)="addUser()" disabled>
            <i class="fa fa-plus"></i>&nbsp;&nbsp;追加
          </button>
          <button class="btn btn-secondary" type="button" (click)="getUsers()">
            <i class="fa fa-refresh"></i>&nbsp;&nbsp;取得
          </button>
          <button class="btn btn-primary" type="submit">
            <i class="fa fa-floppy-o"></i>&nbsp;&nbsp;保存
          </button>
        </div>
      </div>

      <div *ngIf="users">
        <div class="row">
          <div class="col-xs-2">ID</div>
          <div class="col-xs-2">名前</div>
          <div class="col-xs-3">メール</div>
        </div>
        <my-user *ngFor="#user of users" [user]="user"></my-user>
      </div>

      <hr>

      <p>{{ users | json }}</p>

    </form>
  </div>
  `,
  providers: [
    UserService
  ],
  directives: [
    UserComponent
  ]
})
export class UserAdminComponent implements OnInit {

  title: string = "ユーザ管理"
  users: User[];

  constructor(
    private _userService: UserService
  ) {}

  ngOnInit(): void {
    this.getUsers();
  }

  getUsers(): void {
    let source = this._userService.getUsers();
    source.subscribe(data =>
      this.users = <User[]>data
    );
  }

  setUsers(): void {
    if (!this.users) { return; }
    this._userService.setUsers(this.users)
      .subscribe(
        data => console.log(data),
        error => console.error(error)
      );
  }

  addUser(): void {
    this._userService.addUser()
      .subscribe(
        data => this.users.push(<User>data),
        error => console.error(error)
      );
  }

  onSubmit(): void {
    this.setUsers();
  }
}
