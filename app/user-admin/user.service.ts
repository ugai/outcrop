import {Injectable} from 'angular2/core';
import {Http, Response, Headers, RequestOptions} from 'angular2/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/Rx';

import {User} from './user';
import {Utils} from '../common/utils';

import {CLAST_BASE_URL} from '../common/consts';

@Injectable()
export class UserService {

  private _url = CLAST_BASE_URL + '/users';

  constructor(private http: Http) {}

  getUsers(): Observable<any> {

    return this.http.get(this._url)
      .map(res => <User[]> JSON.parse(res.text(), Utils.dateTimeReviver))
      .do(data => console.log(data))
      .catch(this.handleError)
    ;
  }

  private handleError(error: any): Observable<any> {
    console.error(error);
    return Observable.throw(error.json().error || 'Server error');
  }

  addUser(): Observable<any> {
    let body = "";
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });

    return this.http.post(this._url, body, options)
      .map(res => JSON.parse(res.text(), Utils.dateTimeReviver))
      .catch(this.handleError);
  }

  setUsers(users: User[]): Observable<any> {
    let body = JSON.stringify(users); // FIXME: "We may be able to skip the stringify step in the near future."
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });

    return this.http.put(this._url, body, options)
      .map(res => res.text()) // TODO: use JSON
      .catch(this.handleError);
  }
}
