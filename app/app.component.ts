import {Component} from 'angular2/core';
import {RouteConfig, ROUTER_DIRECTIVES} from 'angular2/router';

import {WelcomeComponent} from './welcome.component';
import {AttendanceMainComponent} from './attendance/attendance-main.component';
import {SpentTimeMainComponent} from './spent-time/spent-time-main.component';
import {TestComponent} from './test.component';

@Component({
  selector: 'my-app',
  template: `
  <!-- ナビゲーションバー -->
  <nav class="navbar navbar-fixed-top navbar-dark bg-inverse">
    <a class="navbar-brand" [routerLink]="['Welcome']">
      {{ title }} <small class="text-muted">{{ devStatus }}</small>
    </a>
    <ul class="nav navbar-nav">
      <li class="nav-item"><a class="nav-link" [routerLink]="['Attendance']">勤怠</a></li>
      <li class="nav-item"><a class="nav-link" [routerLink]="['SpentTime']">工数</a></li>
      <li class="nav-item"><a class="nav-link" [routerLink]="['Test']">実験用</a></li>
    </ul>
  </nav>

  <!-- ルータのコンポーネント出力先 -->
  <router-outlet></router-outlet>

  <hr>

  <!-- フッター -->
  <div class="container">
    <footer class="footer">
      <address>
        <p class="text-muted">&copy; 2016 <a target="_blank" href="https://bitbucket.org/ugai/">ugai</a></p>
      </address>
    </footer>
  </div>
  `,
  directives: [
    ROUTER_DIRECTIVES,
    WelcomeComponent,
    AttendanceMainComponent,
    SpentTimeMainComponent,
    TestComponent
  ],
  providers: []
})
@RouteConfig([
  {path: '/',           name: 'Welcome',    component: WelcomeComponent},
  {path: '/kintai',     name: 'Attendance', component: AttendanceMainComponent},
  {path: '/kousu',      name: 'SpentTime',  component: SpentTimeMainComponent},
  {path: '/test',       name: 'Test',       component: TestComponent}
])
export class AppComponent {

  public title = 'Outcrop';
  public devStatus = 'pre-alplha';

}
