var gulp = require('gulp');
var del = require('del');
var uglify = require('gulp-uglify');
var ts = require('gulp-typescript');
var exec = require('child_process').exec;

var tsProject = ts.createProject('tsconfig.json');

var source_dir = './'
var src = {
  app: source_dir + '/app'
}

var public_dir = './dist'
var dist = {
  app: public_dir + '/app',
  lib: public_dir + '/lib'
};

gulp.task('clean_src_files', del.bind(null, [
  src.app + '/**/*.js',
  src.app + '/**/*.map'
]));
gulp.task('clean_app_files', del.bind(null, [
  dist.app + '/**/*.*',
]));
gulp.task('clean_node_files', del.bind(null, [
  dist.lib + '/**/*.*',
]));
gulp.task('clean', ['clean_src_files', 'clean_app_files', 'clean_node_files']);

gulp.task('app_files', ['clean_app_files'], function(){
  var tsResult = gulp.src([src.app + '/**/*.ts']).pipe(ts(tsProject));
  tsResult
    //.pipe(uglify())
    .pipe(gulp.dest(dist.app));
  gulp.src([
      src.app + '/css/**/*.css'
    ], {base: src.app}).pipe(gulp.dest(dist.lib));
});

gulp.task('node_files', ['clean_node_files'], function(){
  gulp.src([
      'node_modules/es6-shim/es6-shim.js',
      'node_modules/systemjs/dist/system-polyfills.js',
      'node_modules/angular2/bundles/angular2-polyfills.js',
      'node_modules/systemjs/dist/system.js',
      'node_modules/rxjs/bundles/Rx.js',
      'node_modules/angular2/bundles/angular2.dev.js',
      'node_modules/angular2/bundles/router.dev.js',
      'node_modules/angular2/bundles/http.dev.js'
    ])
    .pipe(gulp.dest(dist.lib));

  gulp.src([
      'node_modules/bootstrap/dist/css/bootstrap.min.css'
    ], {base: 'node_modules/bootstrap/dist'}).pipe(gulp.dest(dist.lib));

  gulp.src([
      'node_modules/font-awesome/fonts/*.*',
      'node_modules/font-awesome/css/font-awesome.min.css'
    ], {base: 'node_modules/font-awesome'}).pipe(gulp.dest(dist.lib));
});

gulp.task('build', ['app_files', 'node_files']);

gulp.task('watch', ['build'], function () {
  var watcher = gulp.watch([
    src.app + '/**/*.ts',
    src.app + '/**/*.css'
  ], ['app_files']);
  // watcher.on('change', function(event) {
  //   exec('npm run lite -- --baseDir ./ --baseDir ./dist');
  // })
});
